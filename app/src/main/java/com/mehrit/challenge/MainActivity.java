package com.mehrit.challenge;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btn;
    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       editText = (EditText) findViewById(R.id.editText1);


         btn= (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //convert String to Integer
                String a = editText.getText().toString();
              int num= Integer.parseInt(a);
//                /if the input is a number divisible by 3
                if(num%3==0){
                    //output is Foo
                    Toast.makeText(getApplicationContext(),"Foo",Toast.LENGTH_LONG).show();
                }
//        if the input is a number divisible by 5: output "Bar"
                else if(num%5==0){

                    Toast.makeText(getApplicationContext(),"Bar",Toast.LENGTH_LONG).show();

                }
                // if the input is a number divisible by 3 and 5: output "Foobar"
                else if(num%5==0 && num%3==0){
                    Toast.makeText(getApplicationContext(),"FooBar",Toast.LENGTH_LONG).show();

                }
                //nothing
                else{
                    System.out.println(" ");
                }

            }
        });



    }


}
